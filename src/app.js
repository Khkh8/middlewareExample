// app.js

let express = require("express")
let cookieParser = require("cookie-parser")
let app = express()

// logging middleware function
let timeLogger = (req, res, next) => {
    let ms = Date.now()
    console.log(`Request received at ${ms}`)
    next()
}
// requestTime: adds the requestime to req object
let requestTime = (req, res, next) => {
    req.requestTime = Date.now()
    next()
}
// simulate eternal cookie validator
let externallyValidateCookie = (cookie) => {
    return new Promise((resolve, reject) => {
        let wait = setTimeout(() => {
            if ((Date.now()&1) == 0) {
                resolve("Promise is OK")
            } else {
                reject("Promise is not OK")
            }
        }, 1000)
    });    
}


// cookie validator
let cookieValidator = async (cookies) => {
    try {
        await externallyValidateCookie(cookies)
    } catch {
        throw new Error("Invalid cookies received")
    }
}

// middleware for validate cookies
let validateCookies = async (req, res, next) => {
    try {
        await cookieValidator(req.cookies)
        next()
    } catch {
        // reporting async errosr must go through next
        next(new Error("Invalid cookies"))
    }
}

// install middleware into stack
app.use(timeLogger)
app.use(requestTime)

app.use(cookieParser())
app.use(validateCookies)

app.use((err, req, res, next) => {
    console.log("Error logging: "+err.message)
    res.status(400).send(err.message)
})

app.get('/', (req, res) => {
    let responseText = "Hello Express world <br>"
    responseText += `<small>Requested at:${req.requestTime}</small>` 
    res.send(responseText)
})

console.log("Server is listening to port 3000")
app.listen(3000)